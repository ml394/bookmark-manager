#!/usr/bin/python3
import os
import sys
import json
from functools import reduce
from slugify import slugify

class FileManager:

    def __init__(self, bookmarks, folder, verbose=False, *args, **kwargs):
        self.bookmarks = bookmarks
        self.folder = folder
        self.verbose = verbose

    def is_empty(self, item):
        return item == {} or len(item) == 0

    def is_file(self, item):
        if item.get('id', -1) >= 0 and item.get('title') and item.get('url'):
            return True
        return False


class FileWriter(FileManager):

    def create_bookmarks(self, current_dict, current_path):
        for key, value in current_dict.items():
            current_dict = value
            old_path = current_path
            current_path = os.path.join(current_path, key)
            if self.is_file(current_dict) or self.is_empty(current_dict):
                if self.is_file(current_dict):
                    filename = os.path.join(
                        '/'.join(current_path.split('/')[:-1]),
                         '%s.json' % slugify(current_dict['title'])
                    )
                    with open(filename, 'w+') as bookmark_file:
                        bookmark_file.write(
                            json.dumps(current_dict, indent=4, sort_keys=True)
                        )
                    if self.verbose:
                        sys.stdout.write(f'{filename} : bookmark file created\n')
                current_path = old_path
            else:
                if not os.path.isdir(current_path):
                    os.mkdir(current_path)
                if self.verbose:
                    sys.stdout.write(f'{current_path} : folder created\n')
                self.create_bookmarks(current_dict, current_path)


class FileReader(FileManager):

    def read_file(self, path):
        with open(path, 'r') as f:
            return json.load(f)

    def read_bookmarks(self):
        bookmarks = {}
        rootdir = self.folder.rstrip(os.sep)
        start = rootdir.rfind(os.sep) + 1
        for path, dirs, files in os.walk(rootdir):
            folders = path[start:].split(os.sep)
            #subdir = dict.fromkeys(files)
            subdir = dict([
                (f, self.read_file(os.path.join(path, f))) for f in files
            ])
            parent = reduce(dict.get, folders[:-1], bookmarks)
            parent[folders[-1]] = subdir
        return bookmarks

"""
Core classes & functions
"""

import os
import subprocess
import shutil
import json

### Variables ###

cwd = os.getcwd()

class Settings:
    """
    Class defining settings attributes:
      - Folder path
      - HTML filename
      - JSON filename
    """
    FOLDER = os.path.join(cwd, '.bookmarks')
    HTMLFILE = os.path.join(cwd, 'bookmarks.html')
    JSONFILE = os.path.join(cwd, 'bookmarks.json')

settings = Settings

### Class Definitions ###

class Folder:
    """
    Class defining an abstracted folder in the filesystem
    """

    def __init__(self, name, created, modified):
        self.name = name
        self.created = created
        self.modified = modified

    def __str__(self):
        """
        Return folder name as string representation
        """
        return self.name

    def as_dict(self):
        """
        Return class attributes as a dictionary
        """
        return {
            'name': self.name,
            'created': self.created,
            'modified': self.modified
        }

class Bookmark:
    """
    Class defining an abstracted bookmark, that can be converted between
    HTML and JSON notation.
    """

    def __init__(self, _id, title, url, created, modified,
                icon=None, icon_uri=None, parent=None):
        self.id = _id
        self.title = title
        self.url = url
        self.created = created
        self.modified = modified
        self.icon = icon
        self.icon_uri = icon_uri
        self.parent = parent

    def __str__(self):
        """
        Return bookmark title as string representation
        """
        return self.title

    def as_dict(self):
        """
        Return class attributes as a dictionary
        """
        return {
            'id': self.id,
            'title': self.title,
            'url': self.url,
            'created': self.created,
            'modified': self.modified,
            'icon': self.icon,
            'icon_uri': self.icon_uri,
            'parent': self.parent.as_dict() if self.parent else None
        }


class BookmarkProcessor:

    def __init__(
        self,
        folder=".",
        htmlfile=settings.HTMLFILE,
        jsonfile=settings.JSONFILE,
        verbose=False
    ):
        self.folder = folder
        self.htmlfile = htmlfile
        self.jsonfile = jsonfile
        self.verbose = verbose

    def save_json(self, d={'bookmarks': {}}):
        return self.jsonfile.write(json.dumps(d, indent=4, sort_keys=True))


### Function Definitions ###

class Functions:

    def get_parent(child):
        header = child.find_parent('dl').find_previous_sibling('h3')
        if header:
            return (header, Folder(
                name=header.text,
                created=header.attrs.get('add_date'),
                modified=header.attrs.get('last_modified')
            ))
        return None

    def setup_folder(folder=None):
        complete = False
        if not folder:
            settings = Settings()
            folder = settings.FOLDER
        if os.path.isdir(folder):
            shutil.rmtree(folder)
        os.mkdir(folder)
        if os.path.isdir(folder):
            complete = True
        return complete

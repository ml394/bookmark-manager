#!/usr/bin/python3

"""
Create nested folder structure from bookmarks HTML file
"""

from bs4 import BeautifulSoup
import json
from bookmarks.core import (
    Folder,
    Bookmark,
    BookmarkProcessor,
    Functions as functions,
)
from bookmarks.files import FileWriter

class BookmarkImporter(BookmarkProcessor):

    def link_to_bookmark(self, link, num):
        if functions.get_parent(link):
            _parent = functions.get_parent(link)[1]
        else:
            _parent = None
        return Bookmark(
            _id=num,
            title=link.text,
            url=link.attrs.get('href'),
            created=link.attrs.get('add_date'),
            modified=link.attrs.get('last_modified'),
            icon=link.attrs.get('icon'),
            icon_uri=link.attrs.get('icon_uri'),
            parent=_parent
        )

    def html_to_json(self):
        get_parent = functions.get_parent
        bookmarks = {'bookmarks': {}}
        bookmarks_list = []
        errors = []
        html = self.htmlfile
        if html:
            soup = BeautifulSoup(html, 'html.parser')
            links = soup.find_all('a')
            for i, link in enumerate(links):
                bookmark = self.link_to_bookmark(link, i)
                bookmarks_list.append(bookmark)
                directory = {bookmark.title: bookmark.as_dict()}
                child = link
                ascending = True
                while ascending:
                    parent = get_parent(child)
                    if parent:
                        header, folder = parent
                        directory = {folder.name: directory}
                        child = header
                    else:
                        ascending = False
                        try:
                            current_dir = directory
                            current_key = list(current_dir.keys())[0]
                            current_bm = bookmarks['bookmarks']
                            while current_bm.get(current_key):
                                current_bm = current_bm.get(current_key)
                                current_dir = current_dir.get(current_key)
                                current_key = list(current_dir.keys())[0]
                            current_bm.update(current_dir)
                        except Exception as e:
                            errors.append((i, bookmark.as_dict(), e))
        self.save_json(bookmarks)
        return bookmarks, errors

    def write_files(self):
        bookmarks, _errors = self.html_to_json()
        functions.setup_folder(self.folder)
        fw = FileWriter(
            bookmarks=bookmarks['bookmarks'],
            folder=self.folder,
            verbose=self.verbose
        )
        fw.create_bookmarks(current_dict=fw.bookmarks, current_path=fw.folder)
        return True

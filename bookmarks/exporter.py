import sys
from bs4 import BeautifulSoup, Tag
from textwrap import dedent
from bookmarks.core import (
    Folder,
    Bookmark,
    BookmarkProcessor,
    Functions as functions,
    Settings as settings
)
from bookmarks.files import FileReader


class BookmarkExporter(BookmarkProcessor):

    def read_files(self):
        return FileReader(
            bookmarks=None,
            folder=self.folder,
            verbose=self.verbose
        ).read_bookmarks()

    def create_html_template(self):
        base_html = dedent("""
        <!DOCTYPE NETSCAPE-Bookmark-file-1>
        <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
        <TITLE>Bookmarks</TITLE>
        <H1>Bookmarks Menu</H1>
        """)
        soup = BeautifulSoup(base_html, 'html.parser')
        dl = soup.new_tag('DL')
        soup.append(dl)
        return soup

    def write_html(self, soup, dl, bookmarks):
        for key, value in bookmarks.items():
            if value.get('id', -1) >= 0 and value.get('title') and value.get('url'):
                link = soup.new_tag(
                    'A',
                    HREF=value.get('url'),
                    ADD_DATE=value.get('created', ''),
                    LAST_MODIFIED=value.get('modified', ''),
                    #ICON=value.get('icon', ''),
                    #ICON_URI=value.get('icon_uri', '')

                )
                link.string = value.get('title', '')
                dl.append(link)
            else:
                folder_title = soup.new_tag('H3')
                folder_title.string = key
                dl.append(folder_title)
                folder = soup.new_tag('DL')
                dl.append(folder)
                self.write_html(soup, folder, bookmarks.get(key))
        return soup

    def json_to_html(self):
        bookmarks = list(self.read_files().values())[0]
        self.save_json(bookmarks)
        base = self.create_html_template()
        html = self.write_html(base, base.find('DL'), bookmarks)
        return html.prettify()

    def save_html(self):
        html = self.json_to_html()
        self.htmlfile.write(html)
        if self.verbose:
            sys.stdout.write('Boomarks HTML saved\n')
        return True

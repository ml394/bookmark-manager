"""
Bookmark Manager class
"""

from datetime import datetime
from slugify import slugify
import os
import json

from bookmarks.core import (
    Folder,
    Bookmark,
    BookmarkProcessor,
    Functions as functions,
    Settings as settings
)


class BookmarkUpdater(BookmarkProcessor):
    """
    Class defining the Bookmark Updater.
      - Add new bookmarks to the file tree
      - Edit and remove bookmarks in the file tree using the command line
    """

    def __init__(self, title, url, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.title = title
        self.url = url

    def add_bookmark(self):
        bookmark_dict = {
            'title': self.title,
            'url': self.url,
            'id': 1
        }
        filename = os.path.join(self.folder, '%s.json' % slugify(self.title))
        with open(filename, 'w') as bookmark_file:
            bookmark_file.write(
                json.dumps(bookmark_dict, indent=4, sort_keys=True)
            )
        if self.verbose:
            print('Bookmark created: %s' % self.title)

bookmarks
=========

Export bookmarks as HTML from Chrome/Firefox and create a nested folder structure of JSON files representing bookmarks in `~/.bookmarks`. Move, edit & delete bookmarks as JSON files before importing them back into Chrome/Firefox.

## Install

The simplest way to install **bookmark-manager** is through *pip* (coming soon)
```
pip install --user bookmark-manager
```

### From Source

```
git clone https://gitlab.com/ml394/bookmark-manager
cd bookmark-manager
python setup.py install
```

## Usage

```
bookmarks [in|out] [--folder=<folder>|--htmlfile=<htmlfile>|--jsonfile=<jsonfile>]
```

The basic process flow for **bookmark-manager** is as follows:

1. Export your own bookmarks from your browser into an HTML file (see *Appendix*).
2. Run the `bookmarks in` command to create your bookmark file tree.
3. Edit, move and delete bookmarks as you desire.
4. Run the `bookmarks out` command to create a new bookmarks HTML file from your file tree.
5. Import this HTML file into your browser to deploy your changes to Firefox/Chrome

### Options

You can use various options to amend the application's behaviour from its default configuration.

| Option             | Description                                                                          |
|--------------------|--------------------------------------------------------------------------------------|
| `--version`   `-V` | Print **bookmark-manager** release number to console and exit                        |
| `--help`      `-h` | Print **bookmark-manager** help text to console and exit                             |
| `--verbose`   `-v` | Run in verbose mode                                                                  |
| `--folder`    `-F` | The folder path where the bookmarks files will be created (default `./.bookmarks/`)  |
| `--htmlfile`  `-H` | The HTML file to be imported/exported (default `./bookmarks.html`)                   |
| `--jsonfile`  `-J` | The JSON file name to be saved to (default `./bookmarks.json`)                       |

## Contributing

If you're interested in contributing to **bookmark-manager**, please follow these steps:

1. Take a look at the [Contributing Guidelines](CONTRIBUTING.md) and make sure you understand the merge request process
2. Check out ongoing issues in the [Issue List](https://gitlab.com/ml394/bookmark-manager/issues) and see if there's anything you can help out with. Feel free to submit your own issue if you discover a bug or want to suggest a new feature.
3. Clone the `development` branch and checkout your own branch to commit your changes.
4. Push your branch and submit a [Merge Request](https://gitlab.com/ml394/bookmark-manager/merge_requests) for review.

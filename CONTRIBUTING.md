## Contribute to bookmark-manager

Thanks for your interest in contributing to the **bookmark-manager** project.
Take a look at the following steps to get started.

We assume you are working on a machine with a standardized Linux filesystem hierarchy,
and that you have a working Python 2/3 virtual environment set up and activated.

1. Clone the `development` branch and install requirements.
```
git clone -b development https://gitlab.com/ml394/bookmark-manager.git
pip install -r requirements.txt
```
2. Create your own feature/fix branch.
```
git checkout -b <your-branch-name>
```
3. Commit your changes and push your branch.
```
git add -A
git commit -m "This is what I changed"
git push --set-upstream origin <your-branch-name>
```
If your commit provides a fix for a listed issue, prepend your final commit message with `Fix/Resolve/Close #N` (where `N` is the issue number), to automatically close the issue after the approved merge. For example:
```
git commit -m "Fix #99 - error handling in file reader get method"
```
4. Create a merge request from your branch to `development` by visiting the [Merge Requests](https://gitlab.com/ml394/bookmark-manager/merge_requests) page.
5. Wait for your code to be reviewed. Keep an eye on the comment thread.
6. If your merge request is accepted, it will be merged into the staging and master branches for the next full release.

### Testing

You can supply your own bookmarks.html file for testing, or use the basic one contained in the repo at `/tests/docs/bookmarks.html`. The test file is an export of the default bookmarks on Firefox.

Once you have a valid bookmarks HTML export for testing, clone the repo and add the cloned repository's `bin/` folder to `PATH` by adding this line to your `~/.bashrc`. This will allow you to use the `bookmarks` commands in the repository.
```
export PATH=$PATH:/path/to/repo/bin
```
**Please note** this will not work if **bookmark-manager** is already installed in your home folder or virtual environment. Verify that `which bookmarks` is blank before adding the `bin/` folder to `PATH`.

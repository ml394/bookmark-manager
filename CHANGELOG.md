# Changelog
All notable changes to the **bookmark-manager** project will be documented in this file.

## [0.0.2] - 2020-10-07
### Added
- bookmarks.updater module (BookmarkUpdater class)
- add_bookmark function
- new tests

### Changed
- manage_bookmarks script options

### Removed
- N/A

## [0.0.1] - 2019-04-18
### Added
- bookmarks.core module (base settings, functions and classes)
- bookmarks.importer module (BookmarkImporter class)
- bookmarks.files module (FileWriter class)
- Test suite covering all current modules, classes and functions
- Various package files (e.g. this one)

### Changed
- N/A

### Removed
- N/A

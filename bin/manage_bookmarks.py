#!/usr/bin/python3.6

import os
import sys
import argparse
from pprint import pprint
from textwrap import dedent
# Try to import main package. If package can't be found, use ../bookmarks
try:
    from bookmarks.core import Settings as settings
except ImportError:
    import sys
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    from bookmarks.core import Settings as settings

from bookmarks.importer import BookmarkImporter
from bookmarks.exporter import BookmarkExporter
from bookmarks.updater import BookmarkUpdater

VERSION = '0.0.2'

class BookmarkManager:

    def __init__(self):
        self.parser = argparse.ArgumentParser(
            prog='bookmarks',
            description='Manage browser bookmarks in your local filesystem',
            usage=dedent(
                '''
                %(prog)s [-hVv] {in|out|add}
                [-F FOLDER] [-H HTMLFILE] [-J JSONFILE] [-U URL] [-T TITLE]
                '''
            ),
            epilog='',
            formatter_class=argparse.ArgumentDefaultsHelpFormatter
        )
        self.parser.add_argument(
            '-V', '--version', action='version', version=f'%(prog)s {VERSION}'
        )
        self.parser.add_argument(
            '-v', '--verbose', action='store_true', default=False,
            help='Print all actions to console'
        )
        self.parser.add_argument(
            'direction', choices=['in', 'out', 'add'],
            help='Convert HTML to files, or files to HTML'
        )
        self.parser.add_argument(
            '-F', '--folder', type=str, default=settings.FOLDER,
            help='Local bookmarks folder path'
        )
        self.parser.add_argument(
            '-H', '--htmlfile', nargs='?', type=argparse.FileType('r+'),
            default=settings.HTMLFILE, help='HTML file path'
        )
        self.parser.add_argument(
            '-J', '--jsonfile', nargs='?', type=argparse.FileType('a'),
            default=settings.JSONFILE, help='JSON file path'
        )
        self.parser.add_argument(
            '-U', '--url', type=str, help='URL for bookmark being created'
        )
        self.parser.add_argument(
            '-T', '--title', type=str, help='Title of bookmark being created'
        )
        self.options = self.parser.parse_args()

    def run(self):
        if self.options.direction.lower() == 'in':
            importer = BookmarkImporter(
                folder=self.options.folder,
                htmlfile=self.options.htmlfile,
                jsonfile=self.options.jsonfile,
                verbose=self.options.verbose
            )
            importer.write_files()
        elif self.options.direction.lower() == 'out':
            exporter = BookmarkExporter(
                folder=self.options.folder,
                htmlfile=self.options.htmlfile,
                jsonfile=self.options.jsonfile,
                verbose=self.options.verbose
            )
            exporter.save_html()
        elif self.options.direction.lower() == 'add':
            updater = BookmarkUpdater(
                folder=self.options.folder,
                verbose=self.options.verbose,
                url=self.options.url,
                title=self.options.title
            )
            updater.add_bookmark()
        else:
            raise argparse.ArgumentError('Must specify function (in/out/add)')


if __name__=='__main__':
    bm = BookmarkManager()
    bm.run()

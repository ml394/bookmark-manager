"""
Tests for bookmark updater classes and methods
"""

from unittest import TestCase
import os
import json
from slugify import slugify
from bookmarks.updater import BookmarkUpdater
from bookmarks.importer import BookmarkImporter


TEST_DIRECTORY = os.path.join(os.getcwd(), 'tests', 'docs')
TEST_FOLDER = os.path.join(TEST_DIRECTORY, '.bookmarks')
TEST_HTMLFILE = os.path.join(TEST_DIRECTORY, 'bookmarks.html')
TEST_JSONFILE = os.path.join(TEST_DIRECTORY, 'bookmarks.json')


class TestBookmarkUpdater(TestCase):
    """
    Test suite for BookmarkUpdater class
    """

    def setUp(self):
        """
        Setup tests by importing bookmarks then initializing BookmarkUpdater
        """
        htmlfile = open(TEST_HTMLFILE, 'r+')
        jsonfile = open(TEST_JSONFILE, 'a')
        self.importer = BookmarkImporter(
            folder=TEST_FOLDER,
            htmlfile=htmlfile,
            jsonfile=jsonfile,
            verbose=True
        )
        self.importer.write_files()
        self.updater = BookmarkUpdater(
            folder=TEST_FOLDER,
            verbose=True,
            title='Test Bookmark',
            url='http://test/com'
        )

    def test_add_bookmark(self):
        """
        Verify add_bookmark creates a new JSON file in the file tree
        """
        self.updater.add_bookmark()
        try:
            with open(
                os.path.join(TEST_FOLDER, '%s.json' % slugify('Test Bookmark'))
            ) as bookmark_file:
                file_found = True
                bookmark = json.load(bookmark_file)
                bookmark_keys = bookmark.keys()
                self.assertIn('title', bookmark_keys)
                self.assertIn('url', bookmark_keys)
        except FileNotFoundError:
            file_found = False
        self.assertTrue(file_found)

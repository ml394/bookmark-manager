from unittest import TestCase
from datetime import datetime
from bs4 import BeautifulSoup
import os
from bookmarks.core import (
    Folder,
    Bookmark,
    Functions as functions,
    Settings as settings
)

TEST_HTML = """
<dl>
  <h3 add_date="20190101" last_modified="20190202">Test Folder</h3>
  <dl>
    <a href='http://testbookmark.com'>Test Bookmark</a>
  </dl>
</dl>
"""

class TestSettings(TestCase):

    def test_folder(self):
        self.assertEqual(type(settings.FOLDER), type(''))

    def test_htmlfile(self):
        self.assertEqual(type(settings.HTMLFILE), type(''))

    def test_folder(self):
        self.assertEqual(type(settings.JSONFILE), type(''))


class TestFolder(TestCase):

    def setUp(self):
        self.folder = Folder(
            name='Test Folder',
            created=datetime.now(),
            modified=datetime.now()
        )

    def test_str(self):
        self.assertEqual(str(self.folder), self.folder.name)

    def test_as_dict(self):
        self.assertEqual(type(self.folder.as_dict()), type({}))


class TestBookmark(TestCase):

    def setUp(self):
        self.bookmark = Bookmark(
            _id=1,
            title='Test Bookmark',
            url='http://testbookmark.com',
            created=datetime.now(),
            modified=datetime.now()
        )

    def test_str(self):
        self.assertEqual(str(self.bookmark), self.bookmark.title)

    def test_as_dict(self):
        self.assertEqual(type(self.bookmark.as_dict()), type({}))

class TestFunctions(TestCase):

    def test_get_parent(self):
        html = TEST_HTML
        soup = BeautifulSoup(html, "html.parser")
        link = soup.find('a')
        parent = functions.get_parent(link)
        self.assertIsNotNone(parent)
        header, folder = parent
        self.assertEqual(folder.name, 'Test Folder')
        self.assertEqual(folder.created, '20190101')
        self.assertEqual(folder.modified, '20190202')
        self.assertIsNone(functions.get_parent(header))

    def test_setup_folder(self):
        self.assertTrue(functions.setup_folder())
        self.assertTrue(functions.setup_folder())
        os.rmdir('.bookmarks')

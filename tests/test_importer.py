from unittest import TestCase
from bs4 import BeautifulSoup
import os
from bookmarks.core import Settings as settings
from bookmarks.importer import *


TEST_HTML = """
<dl>
  <h3 add_date="20190101" last_modified="20190202">Test Folder</h3>
  <dl>
    <a href='http://testbookmark.com' add_date="20190303" last_modified="20190404">
      Test Bookmark
    </a>
  </dl>
  <a href='http://email.com' add_date="20190505" last_modified="20190606">
    Email
  </a>
</dl>
"""

TEST_DIRECTORY = os.path.join(os.getcwd(), 'tests', 'docs')
TEST_FOLDER = os.path.join(TEST_DIRECTORY, '.bookmarks')
TEST_HTMLFILE = os.path.join(TEST_DIRECTORY, 'bookmarks.html')
TEST_JSONFILE = os.path.join(TEST_DIRECTORY, 'bookmarks.json')


class TestBookmarkImporter(TestCase):

    def setUp(self):
        try:
            with open(TEST_HTMLFILE, 'r') as html_file:
                htmlfile = html_file.read()
        except FileNotFoundError:
            htmlfile = None
        jsonfile = open(TEST_JSONFILE, 'w+')

        self.importer = BookmarkImporter(
            folder=TEST_FOLDER,
            htmlfile=htmlfile,
            jsonfile=jsonfile,
            verbose=True
        )

    def test_htmlfile(self):
        self.assertIsNotNone(self.importer.htmlfile)

    def test_json_file(self):
        self.assertIsNotNone(self.importer.jsonfile)

    def test_link_to_bookmark(self):
        html = TEST_HTML
        soup = BeautifulSoup(html, "html.parser")
        link = soup.find('a')
        bookmark = self.importer.link_to_bookmark(link, 1)
        self.assertEqual(bookmark.id, 1)
        self.assertEqual(bookmark.url, 'http://testbookmark.com')
        self.assertEqual(bookmark.created, '20190303')
        self.assertEqual(bookmark.modified, '20190404')
        self.assertIsNotNone(bookmark.parent)
        link2 = soup.find_all('a')[-1]
        bookmark2 = self.importer.link_to_bookmark(link2, 2)
        self.assertIsNone(bookmark2.parent)

    def test_html_to_json(self):
        blank_result = ({'bookmarks': {}}, [])
        bookmarks, errors = self.importer.html_to_json()
        self.assertTrue(len(bookmarks['bookmarks'].items()) > 0)
        self.assertEqual(len(errors), 0)

    def test_write_files(self):
        self.assertTrue(self.importer.write_files())

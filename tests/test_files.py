from unittest import TestCase
import os
from bookmarks.core import Functions as functions
from bookmarks.files import *

TEST_DIRECTORY = os.path.join(os.getcwd(), 'tests', 'docs')
TEST_FOLDER = os.path.join(TEST_DIRECTORY, '.bookmarks')
TEST_HTMLFILE = os.path.join(TEST_DIRECTORY, 'bookmarks.html')
TEST_JSONFILE = os.path.join(TEST_DIRECTORY, 'bookmarks.json')
BLANK_BOOKMARKS = {'bookmarks': {}}


class TestFileManager(TestCase):

    def setUp(self):
        self.file_manager = FileReader(
            folder=TEST_FOLDER,
            bookmarks=BLANK_BOOKMARKS,
            verbose=True
        )

    def test_verbose(self):
        self.assertTrue(self.file_manager.verbose)

    def test_is_empty(self):
        self.assertTrue(self.file_manager.is_empty({}))
        self.assertTrue(self.file_manager.is_empty([]))
        self.assertFalse(self.file_manager.is_empty({'a': 1}))
        self.assertFalse(self.file_manager.is_empty([1]))

    def test_is_file(self):
        yes_file = {'id': 1, 'title': 'Hello, world', 'url': 'helloworld.com'}
        no_file = {'id': 2, 'some_other_variable': 'whatever'}
        self.assertTrue(self.file_manager.is_file(yes_file))
        self.assertFalse(self.file_manager.is_file(no_file))


class TestFileWriter(TestCase):

    def setUp(self):
        self.file_writer = FileWriter(
            folder=TEST_FOLDER,
            bookmarks=BLANK_BOOKMARKS,
            verbose=True
        )

    def test_create_bookmarks(self):
        self.file_writer.create_bookmarks(
            current_dict=self.file_writer.bookmarks,
            current_path=self.file_writer.folder
        )


class TestFileReader(TestCase):

    def setUp(self):
        self.file_reader = FileReader(
            folder=TEST_FOLDER,
            bookmarks=BLANK_BOOKMARKS,
            verbose=True
        )

    def test_read_bookmarks(self):
        bookmarks = self.file_reader.read_bookmarks()
        if len(bookmarks.keys()) > 0:
            self.assertEqual(list(bookmarks.keys()), ['.bookmarks'])

from unittest import TestCase
import os
from shutil import copyfile
from bookmarks.exporter import *
from bookmarks.importer import BookmarkImporter

TEST_DIRECTORY = os.path.join(os.getcwd(), 'tests', 'docs')
TEST_FOLDER = os.path.join(TEST_DIRECTORY, '.bookmarks')
TEST_HTMLFILE = os.path.join(TEST_DIRECTORY, 'bookmarks.html')
TEST_JSONFILE = os.path.join(TEST_DIRECTORY, 'bookmarks.json')


class TestBookmarkExporter(TestCase):

    def setUp(self):
        htmlfile = open(TEST_HTMLFILE, 'r+')
        jsonfile = open(TEST_JSONFILE, 'w+')
        self.importer = BookmarkImporter(
            folder=TEST_FOLDER,
            htmlfile=htmlfile,
            jsonfile=jsonfile,
            verbose=True
        )
        self.importer.write_files()
        self.exporter = BookmarkExporter(
            folder=TEST_FOLDER,
            htmlfile=htmlfile,
            jsonfile=jsonfile,
            verbose=True
        )

    def test_read_files(self):
        bookmarks = self.exporter.read_files()
        self.assertTrue(len(list(bookmarks.get('.bookmarks', {}).keys())) > 0)

    def test_create_html_template(self):
        self.assertIsNotNone(self.exporter.create_html_template())

    def test_json_to_html(self):
        self.assertIsNotNone(self.exporter.json_to_html())

    def test_save_html(self):
        self.assertTrue(self.exporter.save_html())
        os.remove('tests/docs/bookmarks.html')
        copyfile('tests/docs/bookmarks_copy.html', 'tests/docs/bookmarks.html')
